﻿using Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils;

public static class GenerateJsonWebTokenString
{
    
    public static string GenerateJsonWebToken(this User user,IConfiguration configuration, DateTime now)
    {
        var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration.GetSection("Jwt:SecretKey").Value!));
        var credentials = new SigningCredentials(securityKey,SecurityAlgorithms.HmacSha256);

        var claims = new[]
        {
            new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
            new Claim(ClaimTypes.Name,user.UserName),
            new Claim("id",user.Id.ToString()),
            new Claim(ClaimTypes.Role,"")
             
        };
        var token = new JwtSecurityToken(
            claims: claims,
            expires : now.AddMinutes(15),
            signingCredentials: credentials
            );
        
        return new JwtSecurityTokenHandler().WriteToken(token);
    }
}
