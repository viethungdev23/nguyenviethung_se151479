﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils;

public static class StringUtils
{
    public static string Hash(this string input)
    {
        string password = BCrypt.Net.BCrypt.HashPassword(input);
        return password;
    }
}
