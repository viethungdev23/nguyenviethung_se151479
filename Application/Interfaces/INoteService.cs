﻿using Application.ViewModels.NoteViewModels;
using Application.ViewModels.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces;

public interface INoteService
{
    public Task<List<NoteViewModel>> GetAllNote();
    public Task<List<NoteViewModel>> GetAllNoteByUser(Guid userId);
    public Task<NoteViewModel?> CreateNote(CreateNoteViewModels createNoteViewModels);
    public Task<Response> UpdateNote(String id,UpdateNoteViewModel updateNoteViewModels);
    public Task<List<NoteViewModel>> GetNoteByContainContent(string content);
    Task<Response> DeleteNoteById(Guid id);
}
