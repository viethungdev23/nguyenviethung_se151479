﻿using Application.Commons;
using Application.ViewModels.Response;
using Application.ViewModels.UserViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces;

public interface IUserService
{
    public Task<List<UserViewModel>> GetAllUserAsync();
    public Task<UserViewModel?> Add(CreateUserViewModel userViewModel);
    public Task<Pagination<UserViewModel>> GetUsersPagination(int pageIndex = 0,int pageSize = 10);
    public Task<UserViewModel> GetUserAsync(Guid id);
    public Task<Response> Loginasync(UserLoginDTO userLogin);
    public Task<Response> RegisterAsync(CreateUserViewModel createUserViewModel);
    public Task<UserViewModel> GetUserByUsername(string username);
}
