﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces;

public interface IClaimsService
{
    // ham lay ra id nguoi dung dang su dung
    public Guid GetCurrentUserId { get; }
}
