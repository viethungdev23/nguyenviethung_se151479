﻿using Application.Interfaces;
using Application.ViewModels.RoleViewModels;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services;

public class RoleService : IRoleService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    public RoleService(IUnitOfWork unitOfWork,IMapper mapper)
    {
        _unitOfWork= unitOfWork;
        _mapper= mapper;
    }
    public async Task<RoleViewModel?> AddRole(RoleViewModel roleViewModel)
    {
        var entity = _mapper.Map<Role>(roleViewModel);
        await _unitOfWork.RoleRepository.AddAsyn(entity);
        var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
        if (isSuccess)
        {
            return _mapper.Map<RoleViewModel>(entity);
        }
        return null;
    }
}
