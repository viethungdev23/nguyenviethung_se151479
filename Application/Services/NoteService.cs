﻿using Application.Interfaces;
using Application.ViewModels.NoteViewModels;
using Application.ViewModels.Response;
using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services;

public class NoteService : INoteService
{
    private IUnitOfWork _unitOfWork;
    private IMapper _mapper;
    private IClaimsService _claimsService;
    public NoteService(IUnitOfWork unitOfWork, IMapper mapper, IClaimsService claimsService)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _claimsService = claimsService;
    }

    public async Task<NoteViewModel?> CreateNote(CreateNoteViewModels createNoteViewModels)
    {

        var entity = _mapper.Map<Note>(createNoteViewModels);
        entity.UserId = _claimsService.GetCurrentUserId;
        

        await _unitOfWork.NoteRepository.AddAsyn(entity);
        bool isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
        if(isSuccess)
        {
            return _mapper.Map<NoteViewModel>(entity);
        }
        return null;
    }

    public async Task<Response> DeleteNoteById(Guid id)
    {
       var entity = await _unitOfWork.NoteRepository.GetByIdAsyn(id);
        if(entity == null)
        {
            return new Response(HttpStatusCode.NotFound,"Not found this note");
        }
        _unitOfWork.NoteRepository.SoftRemove(entity);
        await _unitOfWork.SaveChangeAsync();
        return new Response(HttpStatusCode.OK, "Deleted !!", entity);
    }

    public async Task<List<NoteViewModel>> GetAllNote()
    {
        var entities = await _unitOfWork.NoteRepository.GetAllAsync();
        return _mapper.Map<List<NoteViewModel>>(entities);
    }

    public async Task<List<NoteViewModel>> GetAllNoteByUser(Guid userId)
    {
        var entities = await _unitOfWork.NoteRepository.GetAllByUserAsync(userId);
        if(entities is not null)
        {
            return _mapper.Map<List<NoteViewModel>>(entities);
        }
        return null;
    }

    public async Task<List<NoteViewModel>> GetNoteByContainContent(string content)
    {
        var entities = await _unitOfWork.NoteRepository.Find(x => x.Content.Contains(content));
       return _mapper.Map<List<NoteViewModel>>(entities);
    }

    public async Task<Response> UpdateNote(string id, UpdateNoteViewModel updateNoteViewModels)
    {
        //var note = (await _unitOfWork.NoteRepository.Find(x => x.Id == Guid.Parse(id))).FirstOrDefault();
        var note = await _unitOfWork.NoteRepository.GetByIdAsyn(Guid.Parse(id));
        if(note == null) return new Response(HttpStatusCode.NotFound,"Not Found this note!!");

        _mapper.Map(updateNoteViewModels, note);

        _unitOfWork.NoteRepository.Update(note);

        bool isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
        if(isSuccess)
        {
            return new Response(HttpStatusCode.OK,"Update Success!", _mapper.Map<NoteViewModel>(note));
        }
        return new Response(HttpStatusCode.BadRequest,"not Success");
    }
}
