﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.UserViewModels;
using Application.Utils;
using System.Linq;
using AutoMapper;
using Domain.Entities;
using Microsoft.Extensions.Configuration;
using Application.ViewModels.Response;
using System.Net;
using Microsoft.EntityFrameworkCore;

namespace Application.Services;

public class UserService : IUserService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly ICurrentTime _currentTime;
    private IConfiguration _Configuration;
    private readonly ITokenService _tokenService;

    public UserService(IUnitOfWork unitOfWork, IMapper mapper, ICurrentTime currentTime, IConfiguration configuration,ITokenService tokenService)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _currentTime = currentTime;
        _Configuration = configuration;
        _tokenService = tokenService;
    }

    public async Task<UserViewModel?> Add(CreateUserViewModel userViewModel)
    {
        var entity = _mapper.Map<User>(userViewModel);
        await _unitOfWork.UserRepository.AddAsyn(entity);
        var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
        if(isSuccess)
        {
            return _mapper.Map<UserViewModel>(entity);
        }
        return null;
    }

    public async Task<List<UserViewModel>> GetAllUserAsync()
    {
        var users = await _unitOfWork.UserRepository.GetAllAsync();
        var result = _mapper.Map<List<UserViewModel>>(users);
        return result;
    }

    public async Task<Pagination<UserViewModel>> GetUsersPagination(int pageIndex = 0, int pageSize = 10)
    {

        var users = await _unitOfWork.UserRepository.ToPagination(pageIndex, pageSize);
        var result = _mapper.Map<Pagination<UserViewModel>>(users);
        return result;
    }

    public async Task<UserViewModel> GetUserAsync(Guid id)
    {
        var user = await _unitOfWork.UserRepository.GetByIdAsyn(id);
        if (user is null) throw new Exception("not found this user");
        return _mapper.Map<UserViewModel>(user);
    }

    public async Task<Response> Loginasync(UserLoginDTO userLogin)
    {
        var user = await _unitOfWork.UserRepository.GetUserByUserName(userLogin.UserName);
        if (user is null) return new Response(HttpStatusCode.Unauthorized, "Invalid UserName");

        var isValidPass = BCrypt.Net.BCrypt.Verify(userLogin.Password, user.Password);
        if (!isValidPass) return new Response(HttpStatusCode.Unauthorized, "Invalid Password");


        // truyen secretKey and time to grenerate token
        var token = await _tokenService.GetToken(user.UserName);
        if (string.IsNullOrEmpty(token))
        {
            return new Response(HttpStatusCode.Unauthorized, "Invalid password or username");
        }
        var query = _unitOfWork.UserRoleRepository.query();
        query = query.Include(x => x.Role).Where(u => u.UserId == user.Id);
        var userRole = await query.AsNoTracking().FirstOrDefaultAsync();

        LoginResult loginResult = new LoginResult
        {
            UserName = user.UserName,
            Password = user.Password,
            Email = user.Email,
            AvataUrl= user.AvataUrl,
            Token = token,
            Role = userRole.Role.RoleName
        };
        return new Response(HttpStatusCode.OK,"authorized",loginResult);
    }

    public async Task<Response> RegisterAsync(CreateUserViewModel createUserViewModel)
    {
        var isExit = await _unitOfWork.UserRepository.CheckUserNameExited(createUserViewModel.UserName);
        if (isExit)
        {
            return new Response(HttpStatusCode.BadRequest,"UserName already registered!!");
        }
        /*
        User newUser = new User
        {
            UserName = createUserViewModel.UserName,
            Password = createUserViewModel.Password.Hash(),
            Email = createUserViewModel.Email,
            AvataUrl= createUserViewModel.AvataUrl
            // chuwa map 
            
        };
        **/
        createUserViewModel.Password = createUserViewModel.Password.Hash();
        var user = _mapper.Map<User>(createUserViewModel);

        await _unitOfWork.UserRepository.AddAsyn(user);
        var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
        if (isSuccess)
        {
            var role = (await _unitOfWork.RoleRepository.Find(x => x.RoleName == "User")).FirstOrDefault();

            UserRole userRole = new UserRole
            {
                UserId = user.Id,
                RoleId = role.Id
            };
            await _unitOfWork.UserRoleRepository.AddUserRoleAsyn(userRole);
            await _unitOfWork.SaveChangeAsync();
            return new Response(HttpStatusCode.OK, "Registraion success", createUserViewModel);
        }
        return new Response(HttpStatusCode.BadRequest, "Registration failed");
    }

    public async Task<UserViewModel> GetUserByUsername(string username)
    {
        var user = await _unitOfWork.UserRepository.GetUserByUserName(username);
        return _mapper.Map<UserViewModel>(user);
    }
}
