﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.RoleViewModels;

public class RoleViewModel
{
    public string RoleName { get; set; }
}
