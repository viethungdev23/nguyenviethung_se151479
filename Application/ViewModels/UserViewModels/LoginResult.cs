﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.UserViewModels;

public class LoginResult
{
    public string UserName { get; set; }
    public string Password { get; set; }
    public string Email { get; set; }
    public string? AvataUrl { get; set; }
    public string Token { get; set; }
    public string Role { get; set; }
}
