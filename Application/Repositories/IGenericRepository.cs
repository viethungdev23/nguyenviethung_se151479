﻿using Application.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories;

public interface IGenericRepository<T> where T : class
{
    Task<List<T>> GetAllAsync();
    Task<T> GetByIdAsyn(Guid id);
    Task AddAsyn (T entity);
    void Update(T entity);
    Task<List<T>> Find(Expression<Func<T, bool>> expression);
    IQueryable<T> query();
    void UpdateRange(List<T> entities);
    void SoftRemove(T entity);
    Task AddRangeAsync(List<T> entities);
    void SoftRemoveRange(List<T> entities);
    Task<Pagination<T>> ToPagination(int pageNumber = 0, int pageSize = 10);
}
