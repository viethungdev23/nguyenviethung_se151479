﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories;

public interface IUserRoleRepository : IGenericRepository<UserRole>
{
    Task AddUserRoleAsyn(UserRole entity);

    
}
