﻿using Domain.Entities;

namespace Application.Repositories;

public interface IUserRepository : IGenericRepository<User>
{
    // tạo phương thức riêng của user nếu có
    Task<User?> GetUserByUserName(string username);

    Task<bool> CheckUserNameExited(string username);
}
