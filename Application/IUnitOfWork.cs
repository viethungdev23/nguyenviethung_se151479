﻿using Application.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application;

public interface IUnitOfWork
{
    public IUserRepository UserRepository { get; }
    public IRoleRepository RoleRepository { get; }
    public INoteRepository NoteRepository { get; }
    public IUserRoleRepository UserRoleRepository { get; }

    public Task<int> SaveChangeAsync();
}
