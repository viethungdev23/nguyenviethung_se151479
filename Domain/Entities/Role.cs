﻿
namespace Domain.Entities;

public class Role : BaseEntity
{
    public Role()
    {
        this.UserRoles = new HashSet<UserRole>();
    }

    public string RoleName { get; set; }

    public virtual ICollection<UserRole>? UserRoles { get; set; }
}
