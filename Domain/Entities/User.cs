﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities;

public class User : BaseEntity
{
    public User()
    {
        this.UserRoles = new HashSet<UserRole>();
    }
    public string UserName { get; set; }
    public string Password { get; set; }
    public string Email { get; set; }
    public string? AvataUrl { get; set; }

    public virtual ICollection<UserRole>? UserRoles { get; set; }
    public virtual ICollection<Note>? Notes { get; set; }

    
}
