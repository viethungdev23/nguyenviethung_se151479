﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities;

public abstract class BaseEntity
{
    public Guid Id { get; set; } 
    public DateTime CreatedDate { get; set; }
    public Guid? CreatedBy { get; set; }
    public DateTime? UpdatedDate { get; set; }
    public Guid? UpdatedBy { get; set;}
    public DateTime? DeleteDate { get; set; }
    public Guid? DeleteBy { get; set; }
    public bool IsDeleted { get; set; }


}
