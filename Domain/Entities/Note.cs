﻿

namespace Domain.Entities;

public class Note : BaseEntity
{
    public string? Content { get; set; }
    public Guid UserId { get; set; }
    public User User { get; set; }
}
