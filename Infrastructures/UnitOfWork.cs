﻿using Application;
using Application.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures;

public class UnitOfWork : IUnitOfWork
{
    private readonly NoteDbContext _noteDbContext;
    private readonly IUserRepository _userRepository;
    private readonly IRoleRepository _roleRepository;
    private readonly INoteRepository _noteRepository;
    private readonly IUserRoleRepository _userRoleRepository;
    public UnitOfWork(NoteDbContext noteDbContext, IUserRepository userRepository, IRoleRepository roleRepository, INoteRepository noteRepository, IUserRoleRepository userRoleRepository)
    {
        _noteDbContext = noteDbContext;
        _userRepository = userRepository;
        _roleRepository = roleRepository;
        _noteRepository = noteRepository;
        _userRoleRepository = userRoleRepository;
    }
    public IUserRepository UserRepository => _userRepository;

    public IRoleRepository RoleRepository => _roleRepository;

    public INoteRepository NoteRepository => _noteRepository;

    public IUserRoleRepository UserRoleRepository => _userRoleRepository;

    public async Task<int> SaveChangeAsync()
    {
        return await _noteDbContext.SaveChangesAsync();
    }
}
