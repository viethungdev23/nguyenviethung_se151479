﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Infrastructures.FluentAPIs;

public class UserRoleConfiguration : IEntityTypeConfiguration<UserRole>
{
    public void Configure(EntityTypeBuilder<UserRole> builder)
    {
        
        builder.HasKey(x => x.Id);

        builder.HasOne<Role>(r => r.Role)
            .WithMany(ur => ur.UserRoles)
            .HasForeignKey(r => r.RoleId);

        builder.HasOne<User>(r => r.User)
            .WithMany(ur => ur.UserRoles)
            .HasForeignKey(ur => ur.UserId);
    }
}
