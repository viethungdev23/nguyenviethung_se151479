﻿using Application.Commons;
using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories;

public class UserRoleRepository :  IUserRoleRepository
{ 
    private readonly NoteDbContext _noteDbContext;
    public UserRoleRepository(NoteDbContext context, ICurrentTime timeService, IClaimsService claimsService)
    {
        _noteDbContext= context;
    }

    public async Task AddAsyn(UserRole entity)
    {
        throw new NotImplementedException();
        
    }

    public Task AddRangeAsync(List<UserRole> entities)
    {
        throw new NotImplementedException();
    }

    public async Task AddUserRoleAsyn(UserRole entity)
    {
        await _noteDbContext.UserRoles.AddAsync(entity);
    }

    public Task<List<UserRole>> Find(Expression<Func<UserRole, bool>> expression)
    {
        throw new NotImplementedException();
    }

    public Task<List<UserRole>> GetAllAsync()
    {
        throw new NotImplementedException();
    }

    public Task<UserRole> GetByIdAsyn(Guid id)
    {
        throw new NotImplementedException();
    }

    public IQueryable<UserRole> query()
    {
        return _noteDbContext.UserRoles.AsQueryable();
    }

    public void SoftRemove(UserRole entity)
    {
        throw new NotImplementedException();
    }

    public void SoftRemoveRange(List<UserRole> entities)
    {
        throw new NotImplementedException();
    }

    public Task<Pagination<UserRole>> ToPagination(int pageNumber = 0, int pageSize = 10)
    {
        throw new NotImplementedException();
    }

    public void Update(UserRole entity)
    {
        throw new NotImplementedException();
    }

    public void UpdateRange(List<UserRole> entities)
    {
        throw new NotImplementedException();
    }
}
