﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories;

public class UserRepository : GenericRepository<User>, IUserRepository
{
	private readonly NoteDbContext _context;
	public UserRepository(NoteDbContext context,ICurrentTime currentTime,IClaimsService claimsService)
        : base(context,currentTime,claimsService)
	{
		_context= context;
	}

    public Task<bool> CheckUserNameExited(string username)
    {
        // check user exit
       return _context.Users.AnyAsync(u => u.UserName == username);
    }

    public async Task<User?> GetUserByUserName(string username)
    {
        return  _context.Users.FirstOrDefault(record => record.UserName == username); 
    }
}
