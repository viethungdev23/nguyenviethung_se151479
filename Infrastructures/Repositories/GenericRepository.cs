﻿using Application.Commons;
using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Infrastructures.Repositories;

public class GenericRepository<T> : IGenericRepository<T> where T :  BaseEntity
{
    protected DbSet<T> _dbSet;
    private ICurrentTime _timeService;
    private IClaimsService _claimsService;

    public GenericRepository(NoteDbContext context, ICurrentTime timeService, IClaimsService claimsService)
    {
        _dbSet = context.Set<T>();
        _timeService = timeService;
        _claimsService = claimsService;
    }

    public async Task AddAsyn(T entity)
    {
        entity.CreatedDate = _timeService.GetCurrentTime();
        entity.CreatedBy = _claimsService.GetCurrentUserId;
        await _dbSet.AddAsync(entity);
    }

    public async Task AddRangeAsync(List<T> entities)
    {
        foreach(var entity in entities)
        {
            entity.CreatedDate = _timeService.GetCurrentTime();
            entity.CreatedBy = _claimsService.GetCurrentUserId;
        }
        await _dbSet.AddRangeAsync(entities);
    }

    public async Task<List<T>> Find(Expression<Func<T, bool>> expression)
    {
        var data = await _dbSet.Where(expression).ToListAsync();
        return data;
    }

    public Task<List<T>> GetAllAsync()
    {
        return _dbSet.AsNoTracking().ToListAsync();
    }

    public async Task<T> GetByIdAsyn(Guid id)
    {
        var result = _dbSet.FirstOrDefaultAsync(x => x.Id == id);
        if(result is null)
        {
            throw new Exception("not found !!");
        }
        return await result;
    }

    public IQueryable<T> query()
    {
        return _dbSet.AsQueryable();
    }

    public void SoftRemove(T entity)
    {
        entity.IsDeleted = true;
        entity.DeleteDate = _timeService.GetCurrentTime();
        entity.DeleteBy = _claimsService.GetCurrentUserId;
        _dbSet.Update(entity);
    }

    public void SoftRemoveRange(List<T> entities)
    {
        foreach(var entity in entities)
        {
            entity.IsDeleted = true;
            entity.DeleteDate = _timeService.GetCurrentTime();
            entity.DeleteBy = _claimsService.GetCurrentUserId;

        }
        _dbSet.UpdateRange(entities);
    }

    public async Task<Pagination<T>> ToPagination(int pageNumber = 0, int pageSize = 10)
    {
        var itemCount = await _dbSet.CountAsync();
        var item = await _dbSet.Skip(pageNumber * pageSize)
            .Take(pageSize)
            .AsNoTracking()
            .ToListAsync();
        var result = new Pagination<T>()
        {
            PageIndex= pageNumber,
            PageSize = pageSize,
            TotalItemsCount= itemCount,
            Items = item
        };
        return result;
    }

    public void Update(T entity)
    {
        entity.UpdatedDate = _timeService.GetCurrentTime();
        entity.UpdatedBy = _claimsService.GetCurrentUserId;
        _dbSet.Update(entity);
    }

    public void UpdateRange(List<T> entities)
    {
        foreach(var entity in entities)
        {
            entity.UpdatedDate = _timeService.GetCurrentTime();
            entity.UpdatedBy = _claimsService.GetCurrentUserId;
        }
        _dbSet.UpdateRange(entities);
    }
}
