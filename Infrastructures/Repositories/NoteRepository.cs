﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories;

public class NoteRepository : GenericRepository<Note>, INoteRepository
{
	private NoteDbContext _db;
	public NoteRepository(NoteDbContext context,ICurrentTime currentTime,IClaimsService claimsService)
		:base(context,currentTime, claimsService)
	{
		_db= context;
	}

    public async Task<List<Note?>> GetAllByUserAsync(Guid id)
    {
		var notes = _db.Notes.Where(x => x.UserId == id).ToList();
		return notes; 
    }


}
