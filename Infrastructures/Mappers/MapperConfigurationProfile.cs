﻿using Application.Commons;
using Application.ViewModels.NoteViewModels;
using Application.ViewModels.RoleViewModels;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructures.Mappers;

public class MapperConfigurationProfile : Profile
{
	public MapperConfigurationProfile()
	{

		CreateMap(typeof(Pagination<>),typeof(Pagination<>));
		CreateMap<UserLoginDTO, User>();
		CreateMap<CreateUserViewModel, User>();
	    CreateMap<UserViewModel, User>().ReverseMap();
		CreateMap<NoteViewModel, Note>().ReverseMap();
		CreateMap<Note, CreateNoteViewModels>().ReverseMap();
		CreateMap<UpdateNoteViewModel,Note>().IncludeMembers();
        CreateMap<Note, UpdateNoteViewModel>();
        CreateMap<RoleViewModel, Role>().ReverseMap();

	}

    
}
