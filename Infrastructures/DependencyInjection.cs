﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Application.Repositories;
using Infrastructures.Repositories;
using Application.Interfaces;
using Application.Services;
using Application;
using Infrastructures.Mappers;

namespace Infrastructures;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastrucuresService(this IServiceCollection services)
    {
        services.AddScoped<IUnitOfWork, UnitOfWork>();
        services.AddScoped<IUserRepository,UserRepository>();
        services.AddScoped<INoteRepository, NoteRepository>();
        services.AddScoped<IRoleRepository, RoleRepository>();
        services.AddScoped<IUserRoleRepository, UserRoleRepository>();
        services.AddScoped<IUserService, UserService>();
        services.AddScoped<INoteService, NoteService>();
        services.AddScoped<IRoleService, RoleService>();
        services.AddSingleton<ICurrentTime,CurrentTime>();
        services.AddDbContext<NoteDbContext>(option => option.UseSqlServer("Server=.;Database=NoteDB;User Id=sa;Password=12345;Trusted_Connection=True;TrustServerCertificate=true;"));
        services.AddAutoMapper(typeof(MapperConfigurationProfile).Assembly);
        return services;
    }
}
