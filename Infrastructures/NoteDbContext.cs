﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;


namespace Infrastructures;

public class NoteDbContext : DbContext 
{
	public NoteDbContext(DbContextOptions<NoteDbContext> options) : base(options) {}

	public DbSet<User> Users { get; set; }
	public DbSet<Note> Notes { get;set; }
	public DbSet<Role> Roles { get; set; }
	public DbSet<UserRole> UserRoles { get; set; }

}
