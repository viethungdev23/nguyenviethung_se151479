﻿using System.Diagnostics;

namespace NoteApp_02.MiddleWares;

public class PerformanceMiddleware : IMiddleware
{
    private readonly Stopwatch _stopwatch;
    public PerformanceMiddleware(Stopwatch stopwatch)
    {
        _stopwatch = stopwatch;
    }

    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        _stopwatch.Restart();
        _stopwatch.Start();
        Console.WriteLine("Start performce record");
        await next(context);
        Console.WriteLine("end performce record");
        _stopwatch.Stop();
        TimeSpan timeTaken = _stopwatch.Elapsed;
        Console.WriteLine("Time taken : " + timeTaken.ToString(@"m\:ss\.fff"));
    }
}
