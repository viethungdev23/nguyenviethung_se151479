﻿namespace NoteApp_02.MiddleWares;

public class ExceptionMiddleware : IMiddleware
{
    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        try
        {
            await next(context);
        }
        catch (Exception ex)
        {
            Console.WriteLine("exception occur!!");
            Console.WriteLine(ex.Message);
            throw;
        }
    }
}
