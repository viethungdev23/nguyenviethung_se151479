﻿using Application;
using Application.Interfaces;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace NoteApp_02.Services;

public class TokenService : ITokenService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IConfiguration _configuration;

    public TokenService(IUnitOfWork unitOfWork, IConfiguration configuration)
    {
        _unitOfWork = unitOfWork;
        _configuration = configuration;
    }
    public async Task<string> GetToken(string userName)
    {
        var user = (await _unitOfWork.UserRepository.Find(x => x.UserName== userName)).FirstOrDefault();
        if (user is null)
        {
            return null;
        }

        string rolename = "User";

        var query = _unitOfWork.UserRoleRepository.query();
        query = query.Include(x => x.Role).Where(u => u.UserId == user.Id);
        var userRole = await query.AsNoTracking().FirstOrDefaultAsync();

        if (userRole != null)
        {
            rolename = userRole.Role.RoleName;
        }

        var authClaims = new List<Claim>
        {
            new Claim(ClaimTypes.Name,user.UserName),
            new Claim("userID",user.Id.ToString()),
            new Claim("username",user.UserName),
            new Claim(ClaimTypes.Role, rolename)
        };

        var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetSection("Jwt:SecretKey").Value!));
        var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

        var token = new JwtSecurityToken(
            claims: authClaims,
            expires: DateTime.UtcNow.AddMinutes(15),
            signingCredentials: credentials
            );

        return new JwtSecurityTokenHandler().WriteToken(token);
    }
}
