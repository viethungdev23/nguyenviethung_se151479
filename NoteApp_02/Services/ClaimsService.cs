﻿using Application.Interfaces;
using System.Security.Claims;

namespace NoteApp_02.Services;

public class ClaimsService : IClaimsService
{
    
    public ClaimsService(IHttpContextAccessor httpContextAccessor)
    {
       
        var Id = httpContextAccessor.HttpContext?.User?.FindFirstValue("UserID");
        //var currentUserName = httpContextAccessor.HttpContext.User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
        GetCurrentUserId = string.IsNullOrEmpty(Id) ? Guid.Empty : Guid.Parse(Id);
        //var id = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

    }

    public Guid GetCurrentUserId { get;}
}
