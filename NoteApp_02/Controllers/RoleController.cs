﻿using Application.Interfaces;
using Application.ViewModels.RoleViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace NoteApp_02.Controllers;

public class RoleController : BaseController
{
    private readonly IRoleService _roleService;
    public RoleController(IRoleService roleService)
    {
        _roleService= roleService;
    }

    [HttpPost]
    public async Task<RoleViewModel> CreateRole(RoleViewModel model)
    {
        return await _roleService.AddRole(model);
    }
}
