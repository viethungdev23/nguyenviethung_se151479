﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.UserViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace NoteApp_02.Controllers;

public class UserController : BaseController
{
    private readonly IUserService _userService;

    public UserController(IUserService userService)
    {
        _userService = userService;
    }

    [HttpGet]
    [Authorize]
    public async Task<Pagination<UserViewModel>> GetAllUsersPagination(int pageIndex = 0, int pageSize = 10)
    {
        return await _userService.GetUsersPagination(pageIndex, pageSize);
    }

    [HttpGet]
    [Authorize]
    public async Task<UserViewModel> GetUserByUsername(string username)
    {
        return await _userService.GetUserByUsername(username);
    }

    [HttpGet]
    [Authorize]
    public async Task<UserViewModel?> GetUserById(Guid id)
    {
        return await _userService.GetUserAsync(id);

    }
}
