﻿using Application.Interfaces;
using Application.ViewModels.Response;
using Application.ViewModels.UserViewModels;
using Microsoft.AspNetCore.Mvc;

namespace NoteApp_02.Controllers;

public class AuthenticationController : BaseController
{
    private readonly IUserService _userService;
    public AuthenticationController(IUserService userService)
    {
        _userService= userService;
    }

    [HttpPost]
    public async Task<Response> LoginAsync(UserLoginDTO userLogin)
    {
        return await _userService.Loginasync(userLogin);
    }

    [HttpPost]
    public async Task<Response> RegisterAsync(CreateUserViewModel createUserViewModel)
    {
       return await _userService.RegisterAsync(createUserViewModel);
    }
}
