﻿using Application.Interfaces;
using Application.ViewModels.NoteViewModels;
using Application.ViewModels.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace NoteApp_02.Controllers;

[Authorize(Policy = "require")]
//[Authorize(Roles ="User")]
public class NoteController : BaseController
{
	private readonly INoteService _noteService;

	public NoteController(INoteService noteService)
	{
		_noteService = noteService;
	}

	[HttpGet]
	public async Task<List<NoteViewModel>> GetAllNote()
	{
		return await _noteService.GetAllNote();
	}

	[HttpPost]
	public async Task<NoteViewModel?> CreateNote(CreateNoteViewModels createNoteViewModels)
	{
		return await _noteService.CreateNote(createNoteViewModels);
	}

	[HttpPut]
	public async Task<Response> UpdateNote(String id,[FromBody] UpdateNoteViewModel updateNoteViewModels)
	{
		return await _noteService.UpdateNote(id,updateNoteViewModels);
	}

	[HttpGet]
	public async Task<List<NoteViewModel>> GetAllNotesByUser(Guid userid)
	{
		return await _noteService.GetAllNoteByUser(userid);
	}

	[HttpGet]
	public async Task<List<NoteViewModel>> GetNoteByContainContent(String content)
	{
		return await _noteService.GetNoteByContainContent(content);
	}

	[HttpDelete]
	public async Task<Response> DeleteNoteById(Guid id)
	{
		return await _noteService.DeleteNoteById(id);
	}
}
