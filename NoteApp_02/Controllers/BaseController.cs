﻿using Microsoft.AspNetCore.Mvc;

namespace NoteApp_02.Controllers;

[Route("api/[controller]/[action]")]
[ApiController]
public class BaseController : ControllerBase
{
}
